import time

# Introduction
print("Welcome to 'GO TO CHURCH'!\n")
time.sleep(1)
print("It's Sunday morning at 8:35 A.M, you need to go over to church before the devil can corrupt you.")
print("The church is 20 kilometers (20000 meters) away, you have to get there before the mass starts.\n")
print("Player Instructions\n")
print("You start 20000 meters away from church and the devil is 40000 meters away from you.")
print("If the devil catches you it is game over.")
print("You have a water bottle with 10 sips left in it.")
print("Your thirst will return to 0 if you take a sip of water")
print("You cannot jog or sprint if your stamina is at zero you must then rest\n")

# variable definition
distance_left = 20000
car_fuel = 100
devil_distance = 40000
thirst = 0
water = 10
stamina = 100
done = False

print("This is your starting status")
print("Distance left to Church:", distance_left, "meters")
print("The Devil is", devil_distance, "meters behind you")
print("Water Left:", water, "sips")
print("You have:", stamina, "stamina left")
print("Your thirst is at", thirst, "out of 10\n")

while not done:

    # number randomizer
    from random import randint

    if 1 == "B":

        time.sleep(1)
    # turn options
    print('A. Jog to church.')
    time.sleep(.25)
    print("B. Full sprint to church.")
    time.sleep(.25)
    print("C. Drive to church.")
    time.sleep(.25)
    print("D. Stay back and rest.")
    time.sleep(.25)
    print("F. Drink Water")
    time.sleep(.25)
    print("Q. Quit game.")
    time.sleep(.25)

    # Option input
    answer = input("\nWhat do you want to do?:")

    # Option A
    if answer.upper() == "A":
        value = randint(500, 1201)
        print("\nYou jogged", value, "meters\n")
        distance_left -= value
        value = randint(1, 5001)
        devil_distance -= value
        stamina -= 25
        thirst += 1

        if distance_left > 0:
            print("Distance left to Church:", distance_left, "meters")
        else:
            print("Cations! You made it to church before the devil could corrupt to you!")
            break
        if devil_distance > 0:
            print("The Devil is", devil_distance, "meters behind you")
        else:
            print("The devil was able to corrupt you, better luck next time.")
            break
        print("Water Left:", water, "sips")
        print("Your thirst is at", thirst, "out of 10")

    # Option B
    elif answer.upper() == "B":
        value = randint(1000, 2001)
        print("\nYou sprinted", value, 'meters\n')
        distance_left -= value
        value = randint(1, 5001)
        devil_distance -= value
        distance_left = distance_left - value
        stamina -= 50
        thirst += 1

        if distance_left > 0:
            print("Distance left to Church:", distance_left, "meters")
        else:
            print("Congratulations! You made it to church before the devil could corrupt to you!")
            break
        if devil_distance > 0:
            print("The Devil is", devil_distance, "meters behind you")
        else:
            print("The devil was able to corrupt you, better luck next time.")
            break
        print("Water Left:", water, "sips")
        print("Your thirst is at", thirst, "out of 10")

    # Option C
    elif answer.upper() == "C":
        if car_fuel > 0:
            car_fuel -= 100
            value = randint(1000, 4001)
            print("\nYou drove", value, "meters before your car ran out of gas\n")
            distance_left -= value
            value = randint(1, 5001)
            devil_distance -= value
        else:
            print("Your out of fuel")
            if distance_left > 0:
                print("Distance left to Church:", distance_left, "meters")
            else:
                print("Congratulations! You made it to church before the devil could corrupt to you!")
                break
            if devil_distance > 0:
                print("The Devil is", devil_distance, "meters behind you")
            else:
                print("The devil was able to corrupt you, better luck next time.")
                break
        print("Water Left:", water, "sips")
        print("Your thirst is at", thirst, "out of 10")

    # Option D
    if answer.upper() == "D":
        stamina += 50
        print("\nyou rested for 5 minutes")
        print("You are now rested")
        value = randint(1, 5001)
        CRED = '\033[91m'
        CEND = '\033[0m'
        print(CRED + "While you were resting the devil got", value, "meters closer to you!" + CEND)
        devil_distance -= value

        if distance_left > 0:
            print("Distance left to Church:", distance_left, "meters")
        else:
            print("Congratulations! You made it to church before the devil could corrupt to you!")
            break
        if devil_distance > 0:
            print("The Devil is", devil_distance, "meters behind you")
        else:
            print("The devil was able to corrupt you, better luck next time.")
            break
        print("Water Left:", water, "sips")
        if stamina > 0:
            print("You have:", stamina, "stamina left")
        else:
            print("You must rest to keep going")
        print("Your thirst is at", thirst, "out of 10")

    # Option F
    elif answer.upper() == "F":
        if water > 0:
            water -= 1
            thirst = 0
            print("You take a drink")
        else:
            print("you ran out of water.")

    if answer.upper() == "Q":
        print("\nYou've been dragged to hell by the Devil.")
        done = True
        break

    if stamina > 0:
        print("You have:", stamina, "stamina left\n")
    else:
        print("You must rest to keep going")
        print("D. Stay back and rest.")
        print("Q. Quit game.")
        answer = input("\nWhat do you want to do?:")
        if answer.upper() == "D":
            stamina += 50
            print("\nyou rested for 5 minutes")
            print("You are now rested")
            value = randint(1, 5001)
            CRED = '\033[91m'
            CEND = '\033[0m'
            print(CRED + "While you were resting the devil got", value, "meters closer to you!" + CEND)
            devil_distance -= value
