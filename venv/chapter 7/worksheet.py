# QUESTION 1


# String - short for “string of characters,” which normal people think of as text - class str(object):
# Integer - A WHOLE NUMBER - 17
# Floating point - Number of decimals - 2.3849    4 decimals
# Boolean - can only have True Or False value - done = true

# QUESTION 2

my_list = [5, 2, 6, 8, 101]
# prints 2
print(my_list[1])
# prints 101
print(my_list[4])
# error, doesn't print anything because we dont have a 5th number in the list
print(my_list[5])

# QUESTION 3
# prints out my entire list - 5 2 6 8 101
my_list=[5, 2, 6, 8, 101]
for my_item in my_list:
    print(my_item)

# QUESTION 4
my_list1 = [5, 2, 6, 8, 101]
my_list2 = (5, 2, 6, 8, 101)
my_list1[3] = 10
# PRINTS OUT 5, 2, 6, 10, 101 (NO COMMAS)
print(my_list1)
my_list2[2] = 10
# prints out 5, 2, 10, 8, 101
print(my_list2)

# QUESTION 5
my_list = [3 * 5]
#PRINTS 3 * 5
print(my_list)
my_list = [3] * 5
# prints "3" 5 times
print(my_list)

# QUESTION 6
my_list = [5]
for i in range(5):
    my_list.append(i)
# PRINTS 5 THEN PRINTS FROM 1 TO "4" ---> [5, 1, 2, 3, 4,]
print(my_list)

# QUESTION 7
# PRINTS HOW MANY CHARACTERS ARE IN THE BRACKET, INCLUDING SPACES NUMBERS AND EVEN PUNCTUATIONS
# prints 2
print(len("Hi"))
# prints 9
print(len("Hi there."))
# prints 8
print(len("Hi") + len("there."))
# prints 1
print(len("2"))
# either prints 2 or an error because there are no quotations included
print(len(2))

# QUESTION 8
# prints SimpsonCollege
print("Simpson" + "College")
# prints... idk
print("Simpson" + "College"[1])
# prints... im not sure either
print( ("Simpson" + "College")[1] )

# QUESTION 9
word = "Simpson"
for letter in word:
# PRINTS Simpson letter by letter
    print(letter)

# Question 10
word = "Simpson"
for i in range(3):
    word += " College"
print(word)
#writes simpsons once then collage three times

# Question 11
word = "Hi" * 3
print(word)
# prints Hi 3 times in a row

# Question 12
my_text = "The quick brown fox jumped over the lazy dogs."
print("The 3rd spot is: " + my_text[3])
print("The -1 spot is: " + my_text[-1])
# POSITION 3 IS A SPACE POSITION -1 IS THE PERIOD

# Question 13
s = "0123456789"
print(s[1])
print(s[:3])
print(s[3:])
#writes position 1
#writes everything to position 3
#writes everything after poition 3

# Question 14


