print("Welcome to 'Go To Church'!\n")
print("It's Sunday morning at 8:35 A.M, you need to go over to church before the devil can corrupt you.")
print("The church is 5 kilometers away, you have to get there before the mass starts.\n")

# define variables
distance_left = 5000
thirst = 0
car_fuel = 100
devil_distance = 20
water = 1000
stamina = 100
done = False

# turn 1 options
while done == False:
    print("A. Jog to church.")
    print("B. Full sprint to church.")
    print("C. Drive to church.")
    print("D. Stay back and procrastinate.")
    print("E. Status check.")
    print("Q. Quit game.")
    break

# turn 1 choosing option

answer = input("\nWhat do you want to do?:")
if answer == "A":
    print("\nYou jogged 500 meters")
    stamina - 25
    distance_left - 500
if answer == "B":
    print("\nYou sprinted 1000 meters")
    stamina - 50
    distance_left - 1000
if answer == "C":
    print("\nDrive to church")
if answer == "D":
    print("\nyou procrastinated for 5 minutes")
    stamina += 20
if answer =="Q":
    print("\nYou've been dragged to hell by the Devil.")
    done = True
# status check as turn 1
if answer == "E":
        print(("\nDistance left to Church:"), distance_left,"meters")
        print(("The Devil is"),devil_distance,"miles behind you")
        print(("Water Left:"), water,"ml")
        print(("You have:"), stamina,"stamina left\n")

        print("A. Jog to church.")
        print("B. Full sprint to church.")
        print("C. Drive to church.")
        print("D. Stay back and procrastinate.")
        print("Q. Quit game.")

        answer = input("\nWhat do you want to do?:")
        if answer == "A":
            print("\nYou jogged 500 meters")
        stamina - 25
        distance_left - 500
        if answer == "B":
            print("\nYou sprinted 1000 meters")
        stamina - 50
        distance_left - 1000
        if answer == "C":
            print("\nDrive to church")
        if answer == "D":
            print("\nyou procrastinated for 5 minutes")
        stamina += 20
        if answer =="Q":
            print("\nYou've been dragged to hell by the Devil.")
        done = True

print("\nIt's 8:40 A.M, you have 20 minutes before mass starts.")
print(("\nDistance left to Church:"), distance_left,"meters")
print(("The Devil is"),devil_distance,"miles behind you")
print(("Water Left:"), water,"ml")
print(("You have:"), stamina,"stamina left\n")
done = True