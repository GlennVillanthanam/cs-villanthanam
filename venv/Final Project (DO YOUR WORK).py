
import random
import time
''''''' Plot Starts '''''''
print("You appear in the Kanto region with your Articuno.")
print('you look around and see a fellow trainer and go talk to them.\n')
print("Trainer: Hey you! Do you want to battle?\n")
print("\033[4mWe can either battle or have a chat.\033[0m")
time.sleep(.5)
print("\033[96mA. Okay lets battle!")
print("B. No, can we talk?\033[0m")
answer = input("\033[96mWhat do you do?: \033[0m")
if answer == "B":
   print("\nOnly way i'll answer you is if you battle me.")
elif answer.upper() == "A":
    print("\nLets battle then")
print("The trainer used a Charizard, You used your Articuno\n ")

# Game details
print("\033[1mYour Articuno has three abilities Quick attack, Heavy attack, and Heal.")
print("Articuno has 100 HP and can't go over that.")
print("Quick attacks damage can range from 15-25, possible dealing a great deal of damage.")
print("Heavy attack can range from 20-35 damage and a possibly fatal attack.")
print("Heal can help your Articuno recover 20-40 health points.\033[0m \n")

class Pokemon:
    def __init__(self, attack_choice):

        self.__attack_choice = attack_choice


    def attack(self):

        if self.__attack_choice == 1:
            attack_points = random.randint(15,25)
            return attack_points

        elif self.__attack_choice == 2:
            attack_points = random.randint(20,35)
            return attack_points

        else:
            print("That is not a selection. You lost your turn!")

    def heal(self):

        heal_points = random.randint(20,40)
        return heal_points

user_health = 100
charizard_health = 100
snorlax_health = 150
battle_continue = True

while battle_continue == True:
    print("\n\033[1mATTACK CHOICES\033[0m \n1. Quick attack\n2. Heavy attack\n3. Heal")
    attack_choice = eval(input("\n\033[1mSelect an attack:\033[0m "))

    # Charzard selects an attack, but focuses on attacking if health is full.
    if charizard_health == 100:
        charizard_choice = random.randint(1, 2)

    else:
        charizard_choice = random.randint(1, 3)

    charizard = Pokemon(charizard_choice)
    user_pokemon = Pokemon(attack_choice)

    # Attacks by user and Charzard are done simultaneously.
    if attack_choice == 1 or attack_choice == 2:
        damage_to_charizard = user_pokemon.attack()
        heal_self = 0
        print("You dealt",damage_to_charizard,"damage.")

    if charizard_choice == 1 or charizard_choice ==2:
        damage_to_user = charizard.attack()
        heal_charizard = 0
        print("Charizard dealt", damage_to_user, "damage.")

    if attack_choice == 3:
        heal_self = user_pokemon.heal()
        damage_to_charizard = 100
        print("You healed",heal_self,"health points.")

    if charizard_choice == 3:
        heal_charizard = charizard.heal()
        damage_to_user = 0
        print("Charizard healed", heal_charizard, "health points.")

    user_health = user_health - damage_to_user + heal_self
    charizard_health = charizard_health - damage_to_charizard + heal_charizard

    # Pokemon health points are limited by a min of 0 and a max of 100.
    if user_health > 100:
        user_health = 100

    elif user_health <= 0:
        user_health = 0
        battle_continue = False

    if charizard_health > 100:
        charizard_health = 100

    elif charizard_health <= 0:
        charizard_health = 0
        battle_continue = False

    print("Your current health is", user_health)
    print("Charizard's current health is", charizard_health)

print("Your final health is", user_health)
print("Charizard's final health is", charizard_health)

if user_health < charizard_health:

    print("\nYou lost! Better luck next time!")

else:

    print("\nYou won against Charizard!")
    user_health = 100

    while battle_continue == True:
        print("\n\033[1mATTACK CHOICES\033[0m \n1. Quick attack\n2. Heavy attack\n3. Heal")
        attack_choice = eval(input("\n\033[1mSelect an attack:\033[0m "))

    if snorlax_health == 100:
        snorlax_choice = random.randint(1, 2)

    else:
        snorlax_choice = random.randint(1, 3)

    snorlax = Pokemon(snorlax_choice)
    user_pokemon = Pokemon(attack_choice)

    # Attacks by user and Mew are done simultaneously.
    if attack_choice == 1 or attack_choice == 2:
        damage_to_snorlax = user_pokemon.attack()
        heal_self = 0
        print("You dealt",damage_to_snorlax,"damage.")

    if snorlax_choice == 1 or snorlax_choice ==2:
        damage_to_user = snorlax.attack()
        heal_snorlax = 0
        print("Snorlax dealt", damage_to_user, "damage.")

    if attack_choice == 3:
        heal_self = user_pokemon.heal()
        damage_to_snorlax = 0
        print("You healed",heal_self,"health points.")

    if snorlax_choice == 3:
        heal_snorlax = snorlax.heal()
        damage_to_user = 0
        print("Snorlax healed", heal_snorlax, "health points.")

    user_health = user_health - damage_to_user + heal_self
    snorlax_health = snorlax_health - damage_to_snorlax + heal_snorlax

    # Pokemon health points are limited by a min of 0 and a max of 100.
    if user_health > 100:
        user_health = 100

    elif user_health <= 0:
        user_health = 0
        battle_continue = False

    if snorlax_health > 100:
        snorlax_health = 100

    elif snorlax_health <= 0:
        snorlax_health = 0
        battle_continue = False

    print("Your current health is", user_health)
    print("Snorlax's current health is", snorlax_health)

print("Your final health is", user_health)
print("Charizard's final health is", snorlax_health)

if user_health < snorlax_health:

    print("\nYou lost! Better luck next time!")

else:

    print("\nYou won against Snorlax!")
