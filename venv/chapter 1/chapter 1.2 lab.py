
print("Trapezoid Area Calculator")

x1 = float(input("Enter length of top base:"))

x2 = float(input("Enter length of bottom base:"))

h = float(input("Enter height:"))

A = 1 / 2 * (x1 + x2) * h

print(A, 'is the area')