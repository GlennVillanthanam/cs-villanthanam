import time

delay = 0.5

print("Welcome you have come to see the house for sale?")
print

def win():
    time.sleep(delay)
    print("You won!")


def kill():
    time.sleep(delay)
    print("You died!")


def bathroom():
    room_builder([True, False], "the bathroom ", True, ["win", "kill"])


def nook():
    room_builder([False], "the nook", True, ["kill"])


def dining_room():
    room_builder([False, True], "the dining room ", True, ["kill", "win"])


def kitchen():
    room_builder([False, True], "the kitchen ", False, ["kill", "dining_room"])


def bedroom():
    room_builder([True, False], "the bedroom ", False, ["bathroom", "kill"])


def office():
    room_builder([False, True], "the office", False, ["kill", "nook"])



# list of doors(T/F), room name, Last room (T, F)

def room_builder(room_list, room_name, is_end_room, next_rooms):
    print(f"You have just entered {room_name}.")
    time.sleep(delay)
    room_count = len(room_list)
    choices = []

    list_1 = [bathroom, kitchen, office, nook, bedroom, dining_room]
# Dynamically build an input message based on number of doors present
    input_message = "Choose a door: ("

# Add each door name to the list and to a message... eg (door 1, door 2, doo2, door3) IF count + 1 is less than room count
    door_message = ""
    for count in range(room_count):
        choices.append(f"door {count + 1}")
        if count + 1 < room_count:
            door_message += f"door {count + 1}, "
        else:
            door_message += f"door {count + 1}): "
# Add the door message onto the back of the input message
    input_message += door_message
    time.sleep(delay)

    while True:
        command = input(input_message).lower()

        if command in choices:
            if room_list[choices.index(command)]:
                if is_end_room:
                    time.sleep(delay)
                    print("You have made smart decisions!")
                    win()
                    break
                else:
                    time.sleep(delay)
                    print("Please proceed to the next room.")
                    time.sleep(delay)
                    eval(next_rooms[choices.index(command)] + "()")
                    break
            else:
                kill()
                break
        else:
            print(f"Sorry you must enter ({door_message}")

            choice = input('Press q to quit: ')
            if choice == 'q':
                kill()
                break


room_builder([True, True, True], "The Enterance Hall", False, ["kitchen", "bedroom", "office"])