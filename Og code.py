Quiz time!

How many books are there in the Harry Potter series? 7
Correct!

What is 3*(2-1)? 3
Correct!

What is 3*2-1? 5
Correct!

Who sings Black Horse and the Cherry Tree?
1. Kelly Clarkson
2. K.T. Tunstall
3. Hillary Duff
4. Bon Jovi
? 2
Correct!

Who is on the front of a one dollar bill
1. George Washington
2. Abraham Lincoln
3. John Adams
4. Thomas Jefferson
? 2
No.

Congratulations, you got 4 answers right.
That is a score of 80.0 percent.
< Previous
Home
Next >