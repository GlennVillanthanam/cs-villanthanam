import random
import pygame

# Define some colors


BLACK = (0, 0, 0)
WHITE = (255, 255, 255)
GREEN = (0, 255, 0)
RED = (255, 0, 0)
OCEAN = (6, 25, 56)
MOON = (149, 157, 171)
LAND = (33, 105, 14)
DA_MOON = (53, 56, 53)
BROWN = (93, 54, 19)
COMIC = (140, 1, 0)
stars_list_x = []
stars_list_y = []

i = 0


def draw_earth(screen, x, y):
    """ Draw a earth at x, y """
    # EARTH
    pygame.draw.ellipse(screen, OCEAN, [380 + x, 100 + y, 500, 500], )
    # lAND ON EARTH
    pygame.draw.ellipse(screen, LAND, [400 + x, 200 + y, 200, 300], )
    pygame.draw.ellipse(screen, LAND, [600 + x, 400 + y, 120, 90], )
    pygame.draw.ellipse(screen, LAND, [730 + x, 200 + y, 120, 300], )
    pygame.draw.ellipse(screen, LAND, [390 + x, 200 + y, 150, 200], )
    pygame.draw.ellipse(screen, LAND, [390 + x, 300 + y, 150, 200], )
    pygame.draw.ellipse(screen, LAND, [379 + x, 250 + y, 150, 200], )
    pygame.draw.ellipse(screen, LAND, [380 + x, 225 + y, 150, 200], )
    pygame.draw.ellipse(screen, LAND, [383 + x, 275 + y, 150, 200], )
    pygame.draw.rect(screen, LAND, [450 + x, 460 + y, 50, 50], )
    pygame.draw.rect(screen, LAND, [500 + x, 450 + y, 50, 50], )
    pygame.draw.rect(screen, LAND, [450 + x, 190 + y, 50, 50], )


def draw_moon(screen, x, y):
    """ Draw a earth at x, y """
    # MOON
    pygame.draw.ellipse(screen, MOON, [20 + x, 400 + y, 230, 230])
    # CRATERS ON MOON
    pygame.draw.ellipse(screen, DA_MOON, [50 + x, 430 + y, 100, 100], 4)
    pygame.draw.ellipse(screen, DA_MOON, [75 + x, 550 + y, 70, 70], 3)
    pygame.draw.ellipse(screen, DA_MOON, [180 + x, 450 + y, 60, 60], 2)
    pygame.draw.ellipse(screen, DA_MOON, [140 + x, 515 + y, 50, 50], 1)


pygame.init()

# Set the width and height of the screen [width, height]
pygame.display.set_caption("My Game")
size = [1280, 720]
screen = pygame.display.set_mode(size)

# Loop until the user clicks the close button.
done = False

# Used to manage how fast the screen updates
clock = pygame.time.Clock()

# Current position
x_coord = 10
y_coord = 10

# Speed in pixels per frame
x_speed = 0
y_speed = 0

# Current position
x_cord = 10
y_cord = 10
'^^^keyboard^^^'

# Count the joysticks the computer has
joystick_count = pygame.joystick.get_count()
if joystick_count == 0:
    # No joysticks!
    print("Error, I didn't find any joysticks.")
else:
    # Use joystick #0 and initialize it
    my_joystick = pygame.joystick.Joystick(0)
    my_joystick.init()

while not done:

    # ALL EVENT PROCESSING SHOULD GO BELOW THIS COMMENT
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            done = True
        # ALL EVENT PROCESSING SHOULD GO ABOVE THIS COMMENT

        # ALL GAME LOGIC SHOULD GO BELOW THIS COMMENT

        # As long as there is a joystick

        # User pressed down on a key
        elif event.type == pygame.KEYDOWN:
            # Figure out if it was an arrow key. If so
            # adjust speed.
            if event.key == pygame.K_LEFT:
                x_speed = -5
            elif event.key == pygame.K_RIGHT:
                x_speed = 5
            elif event.key == pygame.K_UP:
                y_speed = -5
            elif event.key == pygame.K_DOWN:
                y_speed = 5

        # User let up on a key
        elif event.type == pygame.KEYUP:
            # If it is an arrow key, reset vector back to zero
            if event.key == pygame.K_LEFT or event.key == pygame.K_RIGHT:
                x_speed = 0
            elif event.key == pygame.K_UP or event.key == pygame.K_DOWN:
                y_speed = 0

    if joystick_count != 0:
        # This gets the position of the axis on the game controller
        # It returns a number between -1.0 and +1.0
        horiz_axis_pos = my_joystick.get_axis(0)
        vert_axis_pos = my_joystick.get_axis(1)

        # Move x according to the axis. We multiply by 10
        # to speed up the movement.
        x_coord = x_coord + int(horiz_axis_pos * 10)
        y_coord = y_coord + int(vert_axis_pos * 10)

    # Move the object according to the speed vector.
    x_cord += x_speed
    y_cord += y_speed

    # Draw the stick figure

    # ALL GAME LOGIC SHOULD GO ABOVE THIS COMMENT
    if x_coord < - 380:
        x_coord = - 380

    if x_coord >  400:
        x_coord =  400

    if y_coord < - 100:
        y_coord = - 100

    if y_coord >  120:
        y_coord =  120

    ''''''''''''''''''
    if x_cord < - 20:
        x_cord = - 20

    if x_cord >  1030:
        x_cord =  1030

    if y_cord < - 400:
        y_cord = - 400

    if y_cord >  90:
        y_cord =  90



    # ALL CODE TO DRAW SHOULD GO BELOW THIS COMMENT
    screen.fill(BLACK)

    # First, clear the screen to WHITE. Don't put other drawing commands
    # above this, or they will be erased with this command.


    # Draw the item at the proper coordinates
    draw_earth(screen, x_coord, y_coord)
    draw_moon(screen, x_cord, y_cord)

    # ALL CODE TO DRAW SHOULD GO ABOVE THIS COMMENT

    pygame.display.flip()
    clock.tick(60)

pygame.quit()