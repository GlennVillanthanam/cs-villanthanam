
import pygame

# Define some colors
BLACK = (0, 0, 0)
WHITE = (255, 255, 255)
GREEN = (0, 255, 0)
RED = (255, 0, 0)

pygame.init()

# Set the width and height of the screen [width, height]
screen_size_x = 200
screen_size_y = 200
size = (screen_size_x, screen_size_y)
screen = pygame.display.set_mode(size)

pygame.display.set_caption("My Game")

# Loop until the user clicks the close button.
done = False

# Used to manage how fast the screen updates
clock = pygame.time.Clock()

rectx_speed = 5
recty_speed = 5
rectx = 50
recty = 50

# -------- Main Program Loop -----------
while not done:
    # --- Main event loop
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            done = True

    # --- Game logic should go here

    rectx += rectx_speed
    recty += recty_speed

    if rectx > screen_size_x - 50:
        rectx_speed = rectx_speed * -1
    elif rectx < 0:
        rectx_speed *= -1

    if recty > screen_size_y - 50:
        recty_speed = recty_speed * -1
    elif recty < 0:
        recty_speed *= -1

    # --- Screen-clearing code goes here

    # Here, we clear the screen to white. Don't put other drawing commands
    # above this, or they will be erased with this command.

    # If you want a background image, replace this clear with blit'ing the
    # background image.
    screen.fill(BLACK)

    # --- Drawing code should go here
    pygame.draw.ellipse(screen, RED, [rectx, recty, 50, 50])
    # --- Go ahead and update the screen with what we've drawn.
    pygame.display.flip()

    # --- Limit to 60 frames per second
    clock.tick(60)

# Close the window and quit.
pygame.quit()
