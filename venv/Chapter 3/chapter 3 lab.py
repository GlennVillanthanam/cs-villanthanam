print("Super Hard Quiz\n")

# point collector first part
playing = True
while playing == True:
    point = 0

# Question 1
    print("Question 1 \nWhat is the square root of 5776?  ")
    print("A. 74")
    print("B. 75")
    print("C. 76")
    print("D. 77")
    user_input = input(":")

    C = 76
    c = 76

    if user_input == C or c or 76:
        print("Correct!")
        point += 1
    else:
        print("Incorrect, better luck next time. ")

# Question 2
    user_input = input("\nQuestion 2 \nWho is our Lord and Savior?")

    if user_input == "Jesus" or "jesus" or "God" or "god":
        print("Correct!")
        point += 1
    else:
        print("Incorrect, better luck next time. ")

# Question 3
    user_input = input("\nQuestion 3 \n5318008 ÷ 8 + 666 - 777 × 6 × (3 × 3) = ")

    if user_input == "35890560":
        print("Correct!")
        point += 1
    else:
        print("Incorrect, better luck next time. ")

# Question 4
    user_input = input("\nQuestion 4 \n999 + (432 × 22) - 528 × 31 = ")

    if user_input == "319200":
        print("Correct!")
        point += 1
    else:
        print("Incorrect, better luck next time. ")

# Question 5
    user_input = input("\nQuestion 5 \n600000000 ÷ 3000 + 4500 - 1000 × 3 = ")

    if user_input == "201500":
        print("Correct!")
        point += 1
    else:
        print("Incorrect, better luck next time. ")

# point collector last part
    play_again = input("\nPlay again?")
    if play_again == "yes" or play_again == "Yes":
        playing = True

# Total point and average
    else:
        print("\nGood game, lets play soon. You got", point, "/ 5 correct or", point/5 * 100, "% correct")
        break
