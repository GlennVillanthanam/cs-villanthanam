"""
Example of turning a drawing into a function
Drawing by Paige Lekach
Simplified by Mr. Birrell
"""


import pygame
import math 

#Colours

BLACK    = (   0,   0,   0)
WHITE    = ( 255, 255, 255)
GREEN    = (   0, 255,   0)
RED      = ( 255,   0,   0)
BLUE     = (   0,   0, 255)
SKY_BLUE = (  79, 200, 240)
GRASS_GREEN = ( 12, 145, 63)
BROWN = (153, 102, 0)
KITE_PINK = (255, 153, 255)
KITE_PURPLE = (204, 0, 204)
SUN_YELLOW = (255, 255, 102)
PEACH = (255, 255, 230)
BRIGHT_PINK = (255, 51, 153)
LIGHT_BROWN = (204, 153, 0)
BASKET_BROWN = (102, 51, 0)
BLANKET_RED = (179, 0, 0)
BUSH_GREEN = ( 27, 125, 29)
PI = 3.141592653

pygame.init()

size = (700, 500)
screen = pygame.display.set_mode(size)

pygame.display.set_caption("Create-a-picture")

done = False

clock = pygame.time.Clock()

while not done:
    for event in pygame.event.get():
           if event.type == pygame.QUIT:
               done = True 
               
    screen.fill(GRASS_GREEN)

   
    #Sky
    pygame.draw.rect(screen, SKY_BLUE,[0, 0, 700, 240])
    
    
    #Hill
    pygame.draw.ellipse(screen, GRASS_GREEN, [-5,120,500,300])  
    
      
    #Sun
    pygame.draw.ellipse(screen, SUN_YELLOW, [-14,-10,85,80])
    
    pygame.draw.line(screen,SUN_YELLOW,[0,50], [10,140], 5)
    
    pygame.draw.line(screen,SUN_YELLOW,[40,65], [70,140], 5)    
    
    pygame.draw.line(screen,SUN_YELLOW,[60,40], [120,100], 6)
    
    pygame.draw.line(screen,SUN_YELLOW,[60,20], [150,30], 5)
    
    
    
    #Person    
    pygame.draw.ellipse(screen, PEACH, [450, 200,45,40])
    
    pygame.draw.ellipse(screen, BLACK, [455, 215,5,5])
    
    pygame.draw.arc(screen, GRASS_GREEN, [445,212,20,20],  PI/2,     PI, 2)
    
    pygame.draw.arc(screen, PEACH, [445,212,20,20],     0,   PI/2, 2)
    
    pygame.draw.arc(screen, BLACK,   [445,212,20,20],3*PI/2,   2*PI, 2)
    
    pygame.draw.arc(screen, GRASS_GREEN,  [445,212,20,20],    PI, 3*PI/2, 2)   
    
    pygame.draw.rect(screen, PEACH, [467, 235, 10, 9])
    
    pygame.draw.rect(screen, BLUE, [448, 242, 50, 50])
    
    pygame.draw.rect(screen, BLUE, [435, 255, 17, 9])
    
    pygame.draw.ellipse(screen, PEACH, [425, 255,10,10])
    
    pygame.draw.rect(screen, BLUE, [435, 267, 17, 9])
    
    pygame.draw.ellipse(screen, PEACH, [425, 267,10,10])
    
    pygame.draw.rect(screen, BLACK, [448, 290, 50, 35])
    
    pygame.draw.rect(screen, BLACK, [442, 315, 20, 10]) 
    
    pygame.draw.rect(screen, SUN_YELLOW, [440,198, 60,10])
    
    pygame.draw.rect(screen, SUN_YELLOW, [452, 185, 40,20])
    
          
    #Clouds
    pygame.draw.ellipse(screen, WHITE, [470, 20,90,80])
    
    pygame.draw.ellipse(screen, WHITE, [530, 35,90,80])
    
    pygame.draw.ellipse(screen, WHITE, [490, 60,90,80])
    
     
    
       
    pygame.display.flip()
    
            
    clock.tick(60)    
    

pygame.quit()