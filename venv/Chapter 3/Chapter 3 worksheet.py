#1
temperature = float(input("Temperature: "))
if temperature > 90:
    print("It is hot outside.")
else:
    print("It is not hot out.")
# needed another ) on the end

#2
print("Is your number positive, negative, or zero?")

number = int(input("Enter number here: "))

if number < 0:
    print("You number is negative")
elif number > 0:
         print("You number is positive")
else:
    print("Your number is zero")

#3
number = float(input("Give me a number dude: "))

if number >= -10 and number <= 10:
    print("Success!")

#4
print("A. Dessert topping")
print("B. Desert topping")
user_input = input("A cherry is a: ")
if user_input.upper() == "A":
    print("Correct!")
else:
    print("Incorrect.")
#needed to print options first then user input

#5
x = 4
if x >= 0:
    print("x is positive.")
else:
    print("x is not positive.")
# x had to = signs when it neeeded one and the other problem is that x only equals 4 so the else is unecessary

#6
x = float(input("Enter a number: "))
if x == 3:
    print("You entered 3")
# needed =, and : also needs float

#7
answer = input("What is the name of Dr. Bunsen Honeydew's assistant?: ")
if answer == "Beaker":
    print("Correct!")
else:
    print("Incorrect! It is Beaker.")
# a was supposed to be answer, you needed another = sign, beaker needs (), else doesnt need indent and :

#8
x = input("How are you today?")
if x == "Happy" or "Glad":
    print("That is good to hear!")
# code will always say that is good to hear

#9
#x will be 5 y, wont work, z will equall five, and z will work because y doesnt printing buzz

# mostley right y doesnt work because it is false, and z is true

#10
#prediction line 4 is false, line 5 is false, line 6 prints true, line 7 false, line 8 9 and 10 false, line 11 12 true, 13 false, 14 true

#answers line 4 = true, line 5 = true, line 6 = true, line7 8 = false, 9 = true, 10 11 = false, 12 true, 13 false, 14 true

#11
# line 1 true, line 2 false written weird, line 3 4 5 6 7 true, line 8 9 flase written weird

# line 1 true, line 2 false, line 3 4 5 true, line 6 7 false, line 8 true, line 9 wont run

#12
print("Welcome to Oregon Trail!")

print("A. Banker")
print("B. Carpenter")
print("C. Farmer")

user_input = input("What is your occupation? ")

if user_input.upper() == "A":
    money = 100
elif user_input.upper() == "B":
    money = 70
elif user_input.upper() == "C":
    money = 50

print("Great! you will start the game with", money, "dollars.")

# needed to add .upper() to user_inputs