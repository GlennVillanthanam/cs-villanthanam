"""

Example for turning a drawing into a function
House by Ben Camplin
"""

import pygame

# Define some colors
BLACK = (0, 0, 0)
WHITE = (255, 255, 255)
GREEN = (0, 255, 0)
RED = (255, 0, 0)
BLUE = (0, 0, 255)
TURQUOISE = (24, 201, 178)
ORANGE = (255, 136, 0)
PURPLE = (173, 0, 204)
GREY = (222, 222, 222)
YELLOW = (250, 250, 0)
LIGHTBLUE = (145, 228, 255)
DARKGREEN = (0, 156, 60)

pygame.init()

# Set the width and height of the screen [width, height]
size = (700, 500)
screen = pygame.display.set_mode(size)

pygame.display.set_caption("My Game")

# Loop until the user clicks the close button.
done = False

# Used to manage how fast the screen updates
clock = pygame.time.Clock()


def house(screen, x, y):
    # The house
    pygame.draw.rect(screen, ORANGE, [100 + x - 100, 300 + y - 200, 101, 100], 0)

    pygame.draw.polygon(screen, RED, [[150 + x - 100, 200 + y - 200], [100 + x - 100, 298 + y - 200],
                                      [200 + x - 100, 298 + y - 200]])

    # Window
    pygame.draw.rect(screen, WHITE, [110 + x - 100, 325 + y - 200, 20, 20])

    # Door
    pygame.draw.rect(screen, BLACK, [150 + x - 100, 350 + y - 200, 20, 50])


# -------- Main Program Loop -----------
# Current position
x_coord = 10
y_coord = 10

# Count the joysticks the computer has
gamepad_count = pygame.joystick.get_count()
if gamepad_count == 0:
    # No joysticks!
    print("Error, I didn't find any joysticks.")
else:
    # Use joystick #0 and initialize it
    my_gamepad = pygame.joystick.Joystick(0)
    my_gamepad.init()

while not done:
    # --- Main event loop
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            done = True

    # --- Game logic should go here
    mouse_position = pygame.mouse.get_pos()
    mouse_x = mouse_position[0]
    mouse_y = mouse_position[1]

    gamepad_position = pygame.gamepad.get_pos()
    gamepad_x = gamepad_position[0]
    gamepad_y = gamepad_position[1]
    # --- Drawing code should go here

    # First, clear the screen to white. Don't put other drawing commands
    # above this, or they will be erased with this command.
    screen.fill(WHITE)
    house(screen, mouse_x, mouse_y)

    # --- Go ahead and update the screen with what we've drawn.
    pygame.display.flip()

    # --- Limit to 60 frames per second
    clock.tick(60)

# Close the window and quit.
# If you forget this line, the program will 'hang'
# on exit if running from IDLE.
pygame.quit()