# 1
print("Glenn")

# 2
# put a hashtag and a space and your done

# 3
print(2/3) # divides 2 & 3
print(2//3) # takes out all the remainders from the equation

# 4
import math
print(math.pi)

# 5
# not the same variable in one, A is capitilized, in the other its lower case

# 6
area_of_rectangle = 6
print(area_of_rectangle)
# it uses lower case because its easy and precise

# 7
account#
1Apple
account number
account.number
great.big.variable
2x
total%
#left
# all of these "variables" dont work

# 8
# because print a is written first

# 9
# it does not print anything

# 10
# Radius isnt defined

# 11
# you could get rid of the brackets on the x and y in the string or you could set a = 20 and make it shorter

# 12
# the string needs to be a = 3 * (x + y) to multiply by 3

# 13
#