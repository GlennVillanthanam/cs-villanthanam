import pygame
import random

# Define some colors
BLACK = (0, 0, 0)
WHITE = (255, 255, 255)
GREEN = (0, 255, 0)
RED = (255, 0, 0)
OCEAN = (6, 25, 56)
MOON = (149, 157, 171)
LAND = (33, 105, 14)
DA_MOON = (53, 56, 53)
BROWN = (93, 54, 19)
COMIC = (140, 1, 0)
pygame.init()

# Set the width and height of the screen [width, height]

screen_size_x = 1280
screen_size_y = 720
size = (screen_size_x, screen_size_y)
screen = pygame.display.set_mode(size)

pygame.display.set_caption("My Game")

# Loop until the user clicks the close button.
done = False

# Used to manage how fast the screen updates
clock = pygame.time.Clock()
ELIPY2_speed = 5
ELIPX2_speed = 5
elipx_speed = 5
elipy_speed = 5
elipx = 40
elipy = 40
ELIPX2 = 20
ELIPY2 = 20
boomx = 425
boomy = 402

# -------- Main Program Loop -----------

screen.fill(BLACK)
i = 0
# --- Drawing code should go here
while i < 201:
    position_x = random.randrange(20, 1261)
    position_y = random.randrange(20, 681)
    pygame.draw.ellipse(screen, WHITE, [position_x, position_y, 15, 5], )
    position_x = (position_x + 5)
    position_y = (position_y - 5)
    pygame.draw.ellipse(screen, WHITE, [position_x, position_y, 5, 15], )
    i = (i + 1)

    pygame.draw.ellipse(screen, BROWN, [elipx, elipy, 50, 50])
while not done:
    # --- Main event loop
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            done = True

    # --- Game logic should go here
    elipx += elipx_speed
    elipy += elipy_speed
    ELIPX2 += ELIPX2_speed
    ELIPY2 += ELIPY2_speed

    if elipx > 400 and elipx < 450 and elipy > 365 and elipy < 440:
        elipx_speed = 0
        elipy_speed = 0
        ELIPX2_speed = 0
        ELIPY2_speed = 0



    if elipx > screen_size_x - 50:
        elipx_speed = elipx_speed * -1
    elif elipx < 0:
        elipx_speed *= -1

    if elipy > screen_size_y - 50:
        elipy_speed = elipy_speed * -1
    elif elipy < 0:
        elipy_speed *= -1

    if ELIPX2 > screen_size_x - 50:
        ELIPX2_speed = ELIPX2_speed * -1
    elif ELIPY2 < 0:
        ELIPX2_speed *= -1

    if ELIPY2 > screen_size_y - 50:
        ELIPY2_speed = ELIPY2_speed * -1
    elif ELIPY2 < 0:
        ELIPY2_speed *= -1
    # --- Screen-clearing code goes here

    # Here, we clear the screen to white. Don't put other drawing commands
    # above this, or they will be erased with this command.

    # If you want a background image, replace this clear with blit'ing the
    # background image.

    #EARTH
    pygame.draw.ellipse(screen,OCEAN , [380,100,500,500],)
    #lAND ON EARTH
    pygame.draw.ellipse(screen, LAND, [400, 200, 200, 300], )
    pygame.draw.ellipse(screen, LAND, [600, 400, 120, 90], )
    pygame.draw.ellipse(screen, LAND, [730, 200, 120, 300], )
    pygame.draw.ellipse(screen, LAND, [390, 200, 150, 200], )
    pygame.draw.ellipse(screen, LAND, [390, 300, 150, 200], )
    pygame.draw.ellipse(screen, LAND, [379, 250, 150, 200], )
    pygame.draw.ellipse(screen, LAND, [380, 225, 150, 200], )
    pygame.draw.ellipse(screen, LAND, [383, 275, 150, 200], )
    pygame.draw.rect(screen, LAND, [450, 460, 50, 50], )
    pygame.draw.rect(screen, LAND, [500, 450, 50, 50], )
    pygame.draw.rect(screen, LAND, [450, 190, 50, 50], )
    #MEATEOR
    pygame.draw.ellipse(screen, RED, [ELIPX2, ELIPY2, 50, 50])
    pygame.draw.ellipse(screen, BROWN, [elipx, elipy, 50, 50])
    #MOON
    pygame.draw.ellipse(screen, MOON, [20, 400, 230, 230])
    #CRATERS ON MOON
    pygame.draw.ellipse(screen, DA_MOON, [50, 430, 100, 100], 4)
    pygame.draw.ellipse(screen, DA_MOON, [75, 550, 70, 70], 3)
    pygame.draw.ellipse(screen, DA_MOON, [180, 450, 60, 60], 2)
    pygame.draw.ellipse(screen, DA_MOON, [140, 515, 50, 50], 1)

    #BOOM
    if elipx > 400 and elipx < 450 and elipy > 365 and elipy < 440:
        pygame.draw.rect(screen, COMIC, [390, 350, 465, 200], )
        #B
        pygame.draw.rect(screen, BLACK, [395, 355, 10, 190], )
        pygame.draw.rect(screen, BLACK, [395, 355, 100, 10], )
        pygame.draw.rect(screen, BLACK, [495, 355, 10, 190], )
        pygame.draw.rect(screen, BLACK, [395, 535, 110, 10], )
        pygame.draw.rect(screen, BLACK, [395, 435, 110, 10], )
        #O
        pygame.draw.rect(screen, BLACK, [510, 355, 10, 190], )
        pygame.draw.rect(screen, BLACK, [510, 535, 110, 10], )
        pygame.draw.rect(screen, BLACK, [510, 355, 100, 10], )
        pygame.draw.rect(screen, BLACK, [610, 355, 10, 190], )
        #O
        pygame.draw.rect(screen, BLACK, [625, 355, 10, 190], )
        pygame.draw.rect(screen, BLACK, [625, 535, 110, 10], )
        pygame.draw.rect(screen, BLACK, [625, 355, 100, 10], )
        pygame.draw.rect(screen, BLACK, [725, 355, 10, 190], )
        #M
        pygame.draw.rect(screen, BLACK, [740, 355, 10, 190], )
        pygame.draw.rect(screen, BLACK, [840, 355, 10, 190], )
        pygame.draw.rect(screen, BLACK, [740, 355, 100, 10], )
        pygame.draw.rect(screen, BLACK, [790, 355, 10, 80], )
    # --- Go ahead and update the screen with what we've drawn.
    pygame.display.flip()

    # --- Limit to 60 frames per second
    clock.tick(60)

# Close the window and quit.
pygame.quit()
