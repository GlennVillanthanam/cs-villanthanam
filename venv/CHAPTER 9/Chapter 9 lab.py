import random

#1
def min3(number_1, number_2, number_3):
    if number_1 <= number_2 and number_1 <= number_3:
        return (number_1)
    elif number_2 <= number_1 and number_2 <= number_3:
        return(number_2)
    else:
        return(number_3)
print(min3(4, 7, 5))
print(min3(4, 5, 5))
print(min3(4, 4, 4))
print(min3(-2, -6, -100))
print(min3("Z", "B", "A"))
print("done")
print()

#2
def box(height, width):
    for column in range(height):
        for row in range(width):
            print("*", end=" ")
        print()
box(7, 5)
print()
box(3, 2)
print()
box(3, 10)
print("done")
print()

#3
def find(U, key):
    for item in range(len(U)):
        if U[item] == key:
            print("Found", key, "at position", item)
my_list = [36, 31, 79, 96, 36, 91, 77, 33, 19, 3, 34, 12, 70, 12, 54, 98, 86, 11, 17, 17]
find(my_list, 12)
find(my_list, 91)
find(my_list, 80)
print("done")
print()

#4A
#4.1
def create_list(amount):
    my_list = []
    for b in range(amount):
        number = random.randrange(1,7)
        my_list.append(number)
    return(my_list)
my_list = create_list(5)
print(my_list)
print("done")
print()

#4.2
def count_list(list, number):
    counter = 0
    for a in range(len(list)):
        if list[a] == number:
            counter = (counter + 1)
    return(counter)
count = count_list([1, 2, 3, 3, 3, 4, 2, 1], 3)
print(count)
print("done")
print()

#4.3
def average_list(list):
    list_total = 0
    average_divider = 0
    for g in range(len(list)):
        list_total += list[g]
        average_divider = average_divider + 1
    average = (list_total // average_divider)
    return(average)
average = average_list([1, 2, 3, 4, 5, 6, 7, 8, 9, 10])
print(average)
print("done")
print()

#4B
def main():
    main_list = create_list(10000)
    print(count_list(main_list, 1),"1's in the 10 thousand")
    print(count_list(main_list, 2),"2's in the 10 thousand")
    print(count_list(main_list, 3),"3's in the 10 thousand")
    print(count_list(main_list, 4),"4's in the 10 thousand")
    print(count_list(main_list, 5),"5's in the 10 thousand")
    print(count_list(main_list, 6),"6's in the 10 thousand")
    print("The average is",average_list(main_list))
    print("done")
    print()
main()